#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include "../image_io/image_format.h"

#include <stdint.h>

uint8_t get_file_format(const char* path, enum image_format* format);

#endif //IMAGE_TRANSFORMER_UTILS_H
