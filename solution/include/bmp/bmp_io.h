#ifndef IMAGE_TRANSFORMER_BMP_IO_H
#define IMAGE_TRANSFORMER_BMP_IO_H

#include <stdint.h>
#include <stdio.h>

#include "../image.h"

#include "bmp_header_format.h"
#include "bmp_statuses.h"

#define BMP_FILE_SIGNATURE_LE 0x4D42
#define BMP_FILE_SIGNATURE_BE 0x422D
#define BMP_BI_PLANES 1
#define BMP_BI_SIZE 40
#define BMP_BITS_COUNT 24
#define IMAGE_PADDING_BYTES 4

enum bmp_read_status read_header(FILE* in, struct bmp_header* header);

enum bmp_read_status read_raw_image(FILE* in, struct image* img );

enum bmp_write_status write_header(FILE* out, const struct image* img);

enum bmp_write_status write_raw_image(FILE* out, const struct image* img);

#endif //IMAGE_TRANSFORMER_BMP_IO_H
