#ifndef IMAGE_TRANSFORMER_IMAGE_WRITE_H
#define IMAGE_TRANSFORMER_IMAGE_WRITE_H

#include <stdio.h>

#include "../image.h"

#include "image_format.h"
#include "image_io_statuses.h"

enum write_status write_image(FILE* out, enum image_format format, const struct image* img);

enum write_status write_image_to(const char* path, const struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_WRITE_H
