#ifndef IMAGE_TRANSFORMER_IMAGE_FORMAT_H
#define IMAGE_TRANSFORMER_IMAGE_FORMAT_H

enum image_format {
    IMAGE_FORMAT_BMP = 0,
    IMAGE_FORMAT_PNG,
    IMAGE_FORMAT_JPEG,
    // ...
    IMAGE_FORMAT_NUMBER
};

char* image_format_name(enum image_format format);

#endif //IMAGE_TRANSFORMER_IMAGE_FORMAT_H
