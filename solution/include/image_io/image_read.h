#ifndef IMAGE_TRANSFORMER_IMAGE_READ_H
#define IMAGE_TRANSFORMER_IMAGE_READ_H

#include <stdio.h>

#include "../image.h"

#include "image_format.h"
#include "image_io_statuses.h"


enum read_status read_image(FILE* in, enum image_format format, struct image* img);

enum read_status read_image_from(const char* path, struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_READ_H
