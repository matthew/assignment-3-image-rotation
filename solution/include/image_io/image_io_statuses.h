#ifndef IMAGE_TRANSFORMER_IMAGE_IO_STATUSES_H
#define IMAGE_TRANSFORMER_IMAGE_IO_STATUSES_H

enum read_status {
    READ_OK = 0,

    READ_UNSUPPORTED_TYPE,
    READ_UNSUPPORTED_YET_TYPE,
    READ_WRONG_CONTENT,
    READ_NULL_FILE_POINTER,

    READ_NO_ACCESS,
    READ_NULL_PATH,
    READ_BAD_DESCRIPTOR,
    READ_FILE_BIG,
    READ_IS_DIR,
    READ_FILE_NOT_FOUND,

    READ_FILE_ERROR,

    READ_STATUSES_NUMBER  // must be the last
};

char* read_status_message(enum read_status status);
enum read_status errno_to_read_status(int error_number);

enum write_status {
    WRITE_OK = 0,

    WRITE_UNSUPPORTED_TYPE,
    WRITE_UNSUPPORTED_YET_TYPE,
    WRITE_WRONG_CONTENT,
    WRITE_NULL_FILE_POINTER,
    WRITE_MEMORY_NOT_ALLOCATED,

    WRITE_NO_ACCESS,
    WRITE_NULL_PATH,
    WRITE_BAD_DESCRIPTOR,
    WRITE_FILE_BIG,
    WRITE_IS_DIR,

    WRITE_FILE_ERROR,

    WRITE_STATUSES_NUMBER  // must be the last
};

char* write_status_message(enum write_status status);
enum write_status errno_to_write_status(int error_number);

#endif //IMAGE_TRANSFORMER_IMAGE_IO_STATUSES_H
