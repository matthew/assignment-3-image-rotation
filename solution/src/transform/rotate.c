#include "../../include/transform/rotate.h"
#include "../../include/image_io/image_worker.h"


struct image rotate(const struct image image) {
    struct image res = create_image(image.height, image.width);
    if (!res.data) return res;

    for(int row = 0; row < image.height; row++) {
        for(int col = 0; col < image.width; col++) {
            res.data[col * image.height + image.height - 1 - row] = image.data[row * image.width + col];
        }
    }

    return res;
}
