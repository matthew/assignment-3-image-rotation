#include "../../include/bmp/bmp_io.h"


static uint8_t get_image_padding(const uint64_t img_width, const uint8_t pixel_size) {
    return (IMAGE_PADDING_BYTES - img_width * pixel_size % IMAGE_PADDING_BYTES) % IMAGE_PADDING_BYTES;
}

static uint32_t calc_file_size(const size_t width, const size_t height) {
    size_t padding = (size_t) get_image_padding(width, sizeof(struct pixel));

    size_t size = sizeof(struct bmp_header) + width * height * sizeof(struct pixel) + padding * height;

    if(size > UINT32_MAX) return 0;

    return size;
}

// READ

enum bmp_read_status read_header(FILE* const in, struct bmp_header* const header) {
    // Read header
    if(fread(header, sizeof(struct bmp_header), 1, in) != 1)
        return BMP_READ_INVALID_HEADER;

    // Check header is right
    if(header->bfType != BMP_FILE_SIGNATURE_LE && header->bfType != BMP_FILE_SIGNATURE_BE)
        return BMP_READ_INVALID_SIGNATURE;

    if(header->biBitCount != BMP_BITS_COUNT)
        return BMP_READ_INVALID_BITS;

    // Skip header
    if(fseek(in, (long) header->bOffBits, SEEK_SET) != 0)
        return BMP_READ_INVALID_BITS;

    return BMP_READ_OK;
}

enum bmp_read_status read_raw_image(FILE* const in, struct image* const img ) {
    uint8_t img_padding = get_image_padding(img->width, sizeof(struct pixel));

    // Read image
    for(uint32_t row = 0; row < img->height; row++) {
        if(fread(img->data + row * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            return BMP_READ_INVALID_BITS;
        }
        if(fseek(in, img_padding, SEEK_CUR) != 0) {
            return BMP_READ_INVALID_BITS;
        }
    }

    return BMP_READ_OK;
}

// WRITE

enum bmp_write_status write_header(FILE* const out, const struct image* const img) {
    uint32_t size = calc_file_size(img->width, img->height);

    if (!size || img->width > UINT32_MAX || img->height > UINT32_MAX) return BMP_WRITE_TOO_BIG;

    // Create bmp header
    struct bmp_header header = (struct bmp_header) {
            .bfType = BMP_FILE_SIGNATURE_LE,
            .bfileSize = size,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_BI_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = BMP_BI_PLANES,
            .biBitCount = BMP_BITS_COUNT,
            .biSizeImage = sizeof(struct bmp_header) - size
    };

    // Write header
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
        return BMP_WRITE_ERROR;

    return BMP_WRITE_OK;
}

enum bmp_write_status write_raw_image(FILE* const out, const struct image* const img) {
    uint8_t img_padding = get_image_padding(img->width, sizeof(struct pixel));

    // Write image
    for (int row = 0; row < img->height; row++) {
        if (fwrite(img->data + row * img->width, sizeof(struct pixel), (size_t) img->width, out) != img->width) {
            return BMP_WRITE_ERROR;
        }
        if (fseek(out, img_padding, SEEK_CUR) != 0) return BMP_WRITE_ERROR;
    }

    return BMP_WRITE_OK;
}
