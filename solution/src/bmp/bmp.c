//   ∆~~∆
//  /*\/*\
//  \*/\*/
//  /*\/*\
//  \*/\*/
//  /*\/*\
//  \*/\*/
//   V~~V

#include "../../include/bmp/bmp.h"
#include "../../include/image_io/image_worker.h"

enum bmp_read_status from_bmp( FILE* const in, struct image* const img ) {
    if (!in || !img) return BMP_READ_NULL_IMAGE;

    // Read header
    struct bmp_header header;
    enum bmp_read_status header_read_status = read_header(in, &header);

    if (header_read_status > 0) return header_read_status;

    // Create image
    *img = create_image(header.biWidth, header.biHeight);

    if (!img->data) return BMP_READ_OUT_OF_MEMORY;

    // Read image data
    enum bmp_read_status image_read_status = read_raw_image(in, img);

    if (image_read_status > 0) destroy_image(img);

    return image_read_status;
}


enum bmp_write_status to_bmp(FILE* const out, const struct image* const img ) {
    if (!out || !img) return BMP_WRITE_NULL_IMAGE;

    // Write header
    enum bmp_write_status header_write_status = write_header(out, img);

    if (header_write_status > 0) return header_write_status;

    // Write image data

    return write_raw_image(out, img);
}
