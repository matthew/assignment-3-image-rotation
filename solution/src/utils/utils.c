#include "../../include/utils/utils.h"

#include <string.h>


uint8_t get_file_format(const char* const path, enum image_format* const format) {
    char* ext = strrchr(path, '.');

    if(!ext) return 1;
    ext++;

    if (!strcmp(ext, image_format_name(IMAGE_FORMAT_BMP))) {
        *format = IMAGE_FORMAT_BMP;
    } else if (!strcmp(ext, image_format_name(IMAGE_FORMAT_PNG))) {
        *format = IMAGE_FORMAT_PNG;
    } else if (!strcmp(ext, image_format_name(IMAGE_FORMAT_JPEG))) {
        *format = IMAGE_FORMAT_JPEG;
    } else {
        return 1;
    }

    return 0;
}
