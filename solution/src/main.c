#include <stdio.h>

#include "../include/transform/rotate.h"

#include "../include/image_io/image_io.h"
#include "../include/image_io/image_worker.h"

int main( int argc, char** argv ) {
    if (argc < 3) {
        if (argc == 1)
            printf("Please, pass the input file in first argument and output file in second argument");
        if (argc == 2)
            printf("Please, pass output file in second argument");

        return 1;
    }

    struct image image;

    const enum read_status read_status = read_image_from(argv[1], &image);

    if(read_status > 0) {
        fprintf(stderr, "%s\n", read_status_message(read_status));
        return read_status;
    }

    struct image result = rotate(image);

    const enum write_status write_status = result.data ?
            write_image_to(argv[2], &result) : WRITE_MEMORY_NOT_ALLOCATED;

    // Clear images data
    destroy_image(&image);
    destroy_image(&result);

    if(write_status > 0) {
        fprintf(stderr, "%s\n", write_status_message(write_status));
        return write_status;
    }

    return 0;
}
