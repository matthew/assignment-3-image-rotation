# include "../../include/image_io/image_format.h"

#include <stddef.h>

char* image_format_name(const enum image_format format) {
    char* IMAGE_FORMATS_NAMES[IMAGE_FORMAT_NUMBER] = {
            "bmp",
            "png",
            "jpeg"
    };

    if(format >= IMAGE_FORMAT_NUMBER) return NULL;

    return IMAGE_FORMATS_NAMES[format];
}
