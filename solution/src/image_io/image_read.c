#include "../../include/image_io/image_read.h"
#include "../../include/bmp/bmp.h"
#include "../../include/utils/utils.h"

#include <errno.h>


enum read_status read_image(FILE* const in, const enum image_format format, struct image* const img) {
    if (!in) return READ_NULL_FILE_POINTER;

    // Check formats
    if (format == IMAGE_FORMAT_BMP) {
        const enum bmp_read_status read_status = from_bmp(in, img);

        if (read_status > 0) {
            fprintf(stderr, "BMP Read error: %s\n", bmp_read_status_message(read_status));

            return READ_WRONG_CONTENT;
        }
    } else if (format == IMAGE_FORMAT_PNG || format == IMAGE_FORMAT_JPEG) {
        return READ_UNSUPPORTED_YET_TYPE;
    } else {
        return READ_UNSUPPORTED_TYPE;
    }

    return READ_OK;
}


enum read_status read_image_from(const char* const path, struct image* const img) {
    if (!path) return READ_NULL_PATH;

    // Read file format
    enum image_format format;
    if (get_file_format(path, &format) > 0) return READ_UNSUPPORTED_TYPE;

    // Open file
    FILE* in = fopen(path, "rb");
    if (!in) return errno_to_read_status(errno);

    // Read image
    enum read_status res = read_image(in, format, img);

    fclose(in);
    return res;
}
