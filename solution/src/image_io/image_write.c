#include "../../include/image_io/image_write.h"
#include "../../include/bmp/bmp.h"
#include "../../include/utils/utils.h"

#include <errno.h>


enum write_status write_image(FILE* const out, const enum image_format format, const struct image* const img) {
    if(!out) return WRITE_NULL_FILE_POINTER;

    // Check formats
    if (format == IMAGE_FORMAT_BMP) {
        const enum bmp_write_status write_status = to_bmp(out, img);

        if (write_status > 0) {
            fprintf(stderr, "BMP Write error: %s\n", bmp_write_status_message(write_status));

            return WRITE_WRONG_CONTENT;
        }
    } else if (format == IMAGE_FORMAT_PNG || format == IMAGE_FORMAT_JPEG) {
        return WRITE_UNSUPPORTED_YET_TYPE;
    } else {
        return WRITE_UNSUPPORTED_TYPE;
    }

    return WRITE_OK;
}


enum write_status write_image_to(const char* const path, const struct image* const img) {
    if (!path) return WRITE_NULL_PATH;

    // Read file format
    enum image_format format;
    if (get_file_format(path, &format) > 0) return WRITE_UNSUPPORTED_TYPE;

    FILE* out = fopen(path, "wb+");
    if(!out) return errno_to_write_status(errno);

    enum write_status res = write_image(out, format, img);

    fclose(out);
    return res;
}
