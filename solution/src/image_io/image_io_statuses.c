#include "../../include/image_io/image_io_statuses.h"
#include <errno.h>

char* read_status_message(const enum read_status status) {
    char * READ_STATUS_ERRS[READ_STATUSES_NUMBER] = {
            "Successful read",
            "Unsupported image type!",
            "This image format isn't yet supported! But it will be soon",
            "Reading has failed during parsing file",
            "ERROR! File pointer equals to NULL",
            "Unable to open file: no access",
            "Unable to open file: path is empty",
            "Unable to open file: bad file descriptor",
            "Unable to open file: file too big",
            "Unable to open file: file is dir",
            "Unable to open file: file not found",
            "Read failed"
    };

    if(status >= READ_STATUSES_NUMBER)
        return "Read error!";

    return READ_STATUS_ERRS[status];
}

enum read_status errno_to_read_status(const int error_number) {
    switch (error_number) {
        case EACCES:
            return READ_NO_ACCESS;
        case EBADF:
            return READ_BAD_DESCRIPTOR;
        case EFBIG:
            return READ_FILE_BIG;
        case EISDIR:
            return READ_IS_DIR;
        case ENOENT:
            return READ_FILE_NOT_FOUND;
        default:
            return READ_FILE_ERROR;
    }
}

char* write_status_message(const enum write_status status) {
    char * WRITE_STATUS_ERRS[WRITE_STATUSES_NUMBER] = {
            "Successfully write",
            "Unsupported image type!",
            "This image format isn't yet supported! But it will be soon",
            "Writing has failed during parsing file",
            "ERROR! File pointer equals to NULL",
            "Not enough memory to write result image! ",
            "Unable to open file: no access",
            "Unable to open file: path equals to NULL",
            "Unable to open file: bad file descriptor",
            "Unable to open file: file too big",
            "Unable to open file: file is dir",
            "Write failed"
    };

    if(status >= WRITE_STATUSES_NUMBER)
        return "Write error!";

    return WRITE_STATUS_ERRS[status];
}

enum write_status errno_to_write_status(const int error_number) {
    switch (error_number) {
        case EACCES:
            return WRITE_NO_ACCESS;
        case EBADF:
            return WRITE_BAD_DESCRIPTOR;
        case EFBIG:
            return WRITE_FILE_BIG;
        case EISDIR:
            return WRITE_IS_DIR;
        default:
            return WRITE_FILE_ERROR;
    }
}
